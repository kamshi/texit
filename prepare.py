#!/usr/bin/python

import os
from sys import platform
import subprocess

print("Creating build directories")
if platform == "linux" or platform == "linux2":
    if not os.path.exists('build/Debug'):
        os.makedirs('build/Debug')
    if not os.path.exists('build/Release'):
        os.makedirs('build/Release')
elif platform == "win32":
    if not os.path.exists('build'):
        os.makedirs('build')
else:
    print("Unsupported platform")
    exit(-1)

print("Running cmake")
def runCMake():
    if platform == "win32":
        p = subprocess.Popen(['cmake', '-G', 'Visual Studio 17 2022', '..'], cwd='build')
        p.wait()
    elif platform == "linux" or platform == "linux2":
        p = subprocess.Popen(['cmake', '-DCMAKE_BUILD_TYPE=Debug', '../..'], cwd='build/Debug')
        p.wait()
        p = subprocess.Popen(['cmake', '-DCMAKE_BUILD_TYPE=Release', '../..'], cwd='build/Release')
        p.wait()
    elif platform == "darwin":
        raise RuntimeError

runCMake()
