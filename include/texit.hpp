﻿/*
 * \file texit.hpp
 * \copyright Copyright (c) 2022 Sebastian Jähne
 */

#pragma once

// os
#ifdef WIN32
#include <Windows.h>
#endif

// stl
#include <cassert>
#include <cstdlib>
#include <filesystem>
#include <format>
#include <fstream>
#include <optional>
#include <sstream>
#include <string_view>
#include <system_error>
#include <utility>
#include <vector>

namespace texit
{

enum class InputFormat : unsigned
{
    TeX = 0,
    LaTeX,
    XeTeX,
    XeLaTeX,
    LuaTeX,
    LuaLaTeX,
    NumFormats
};

enum class OutputFormat : unsigned
{
    DVI = 0,
    PDF,
    NumFormats
};

class Result
{
public:
    enum class ErrorCode
    {
        noError,
        ioError,
        execFailed
    };

    Result(const ErrorCode errorCode, const std::string& description) :
        errorCode(errorCode), description(description)
    {}

    operator bool() const
    {
        return errorCode == ErrorCode::noError;
    }

    auto getDescription() const -> std::string_view
    {
        return description;
    }

private:
    ErrorCode errorCode = ErrorCode::noError;
    std::string description;
};

inline constexpr auto commandFor(const InputFormat inputFormat,
                                 const OutputFormat outputFormat) -> std::string_view
{
    switch(inputFormat)
    {
    case InputFormat::TeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "tex";
        case OutputFormat::PDF:
            return "pdftex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::LaTeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "latex";
        case OutputFormat::PDF:
            return "pdflatex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::XeTeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "";
        case OutputFormat::PDF:
            return "xetex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::XeLaTeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "";
        case OutputFormat::PDF:
            return "xelatex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::LuaTeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "";
        case OutputFormat::PDF:
            return "luatex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::LuaLaTeX:
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return "";
        case OutputFormat::PDF:
            return "lualatex";
        case OutputFormat::NumFormats:
            assert(false);
            return "";
        }
        break;
    case InputFormat::NumFormats:
        assert(false);
        return "";
    }
    return "";
}

#ifdef WIN32
int spawnProcessWithoutConsole(const std::string& command)
{
    // Initialize the STARTUPINFO structure
    STARTUPINFO si;
    ZeroMemory(&si, sizeof(si));
    si.cb = sizeof(si);

    // Initialize the PROCESS_INFORMATION structure
    PROCESS_INFORMATION pi;
    ZeroMemory(&pi, sizeof(pi));

    // Set the flags to prevent a console window from appearing
    si.dwFlags = STARTF_USESHOWWINDOW;
    si.wShowWindow = SW_HIDE;

    // Create the process
    if(CreateProcess(NULL,                               // No module name (use command line)
                     const_cast<LPSTR>(command.c_str()), // Command line
                     NULL,                               // Process handle not inheritable
                     NULL,                               // Thread handle not inheritable
                     FALSE,                              // Set handle inheritance to FALSE
                     CREATE_NO_WINDOW,                   // Do not create a console window
                     NULL,                               // Use parent's environment block
                     NULL,                               // Use parent's starting directory
                     &si,                                // Pointer to STARTUPINFO structure
                     &pi                                 // Pointer to PROCESS_INFORMATION structure
                     ))
    {
        // Wait until the process exits
        WaitForSingleObject(pi.hProcess, INFINITE);

        // Get the exit code
        DWORD exitCode;
        if(GetExitCodeProcess(pi.hProcess, &exitCode))
        {
            // Close process and thread handles
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            return static_cast<int>(exitCode);
        }
        else
        {
            // Close process and thread handles
            CloseHandle(pi.hProcess);
            CloseHandle(pi.hThread);
            return -1; // Indicate failure to retrieve exit code
        }
    }
    else
    {
        return -1; // Indicate failure to create process
    }
}
#endif

inline auto makeSystemCall(const std::string_view executable, const std::string_view arguments)
{
    std::stringstream toRun;
    toRun << executable << ' ' << arguments;
#ifdef _WIN32
    toRun << " >NUL 2>NUL"; // prevent console output
#elif defined __linux__
    toRun << " &>/dev/null"; // prevent console output
#endif
    return std::system(toRun.str().c_str());
}

// result, temporary file name
[[nodiscard]] inline auto
convertSourceToTempFile(const std::string_view source, const InputFormat inputFormat,
                        const OutputFormat outputFormat) -> std::pair<Result, std::filesystem::path>
{
    // determine the command to run
    const auto commandName = commandFor(inputFormat, outputFormat);

    // create temporary directory
    std::error_code errorCode;
    const auto temporaryDirectory =
        std::filesystem::canonical(std::filesystem::temp_directory_path(errorCode));
    if(errorCode.value() != 0)
        return { Result(Result::ErrorCode::ioError, "Could not create temporary directory"), {} };

    const auto sourceFilename = temporaryDirectory / "texput.tex";
    const auto resultFilename = [&outputFormat, &temporaryDirectory]()
    {
        switch(outputFormat)
        {
        case OutputFormat::DVI:
            return temporaryDirectory / "texput.dvi";
        case OutputFormat::PDF:
            return temporaryDirectory / "texput.pdf";
        default:
            return temporaryDirectory / "not_found";
        }
    }();

    // create temporary source file
    {
        std::ofstream sourceFile(sourceFilename);
        if(!sourceFile.is_open())
            return { Result(Result::ErrorCode::ioError, "Could not create temporary source file"),
                     {} };

        sourceFile << source << std::flush;
        sourceFile.close();
    }

    // run command
    std::stringstream arguments;
    arguments << "-interaction=batchmode -halt-on-error -file-line-error -no-shell-escape"
              << " -aux-directory=\"" << temporaryDirectory.string() << "\" -output-directory=\""
              << temporaryDirectory.string() << "\" \"" << sourceFilename.string() << '"';
#ifdef WIN32
    const auto exitCode =
        spawnProcessWithoutConsole(std::format("{} {}", commandName, arguments.str()));
#else
    const auto exitCode = makeSystemCall(commandName, arguments.str());
#endif
    if(exitCode != 0)
        return { Result(Result::ErrorCode::execFailed,
                        std::format("Process returned non-zero exit code. Command:\n{} {}",
                                    commandName, arguments.str())),
                 {} };

    return { Result(Result::ErrorCode::noError, ""), resultFilename };
}

[[nodiscard]] inline auto convertSourceToBytes(
    const std::string_view source, const InputFormat inputFormat,
    const OutputFormat outputFormat) -> std::pair<Result, std::optional<std::vector<std::uint8_t>>>
{
    const auto [result, resultFilename] =
        convertSourceToTempFile(source, inputFormat, outputFormat);
    if(!result)
        return { result, {} };

    std::ifstream resultFile(resultFilename, std::ios_base::binary);
    if(!resultFile.is_open())
        return { Result(Result::ErrorCode::ioError, "Could not read temporary result file"), {} };

    const auto fileSize = std::filesystem::file_size(resultFilename);
    std::vector<std::uint8_t> bytes(fileSize);
    resultFile.read(reinterpret_cast<char*>(bytes.data()), fileSize);
    return { Result(Result::ErrorCode::noError, ""), std::move(bytes) };
}

[[nodiscard]] inline auto
convertSourceToFile(const std::string_view source, const InputFormat inputFormat,
                    const std::string_view destinationFilename, const OutputFormat outputFormat,
                    std::filesystem::copy_options copyOptions = {}) -> Result
{
    const auto [result, resultFilename] =
        convertSourceToTempFile(source, inputFormat, outputFormat);
    if(!result)
        return result;

    std::error_code errorCode;
    std::filesystem::copy_file(resultFilename, destinationFilename, copyOptions, errorCode);
    if(errorCode.value() != 0)
        return Result(Result::ErrorCode::ioError, "Could not copy output file");

    return Result(Result::ErrorCode::noError, "");
}

}
