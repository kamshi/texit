#!/usr/bin/python

from pathlib import Path
import subprocess
import os

def formatSources():
	headerDirectories = []
	headerFiles = []
	sourceFiles = []
	for path in Path('.').rglob('*.hpp'):
		filename = path.as_posix()
		if not filename.startswith("build"):
			headerDirectories.append(path.parent)
			headerFiles.append(filename)
	for path in Path('.').rglob('*.cpp'):
		filename = path.as_posix()
		if not filename.startswith("build"):
			sourceFiles.append(filename)

	headerDirectories = list(dict.fromkeys(headerDirectories))

	compilerIncludePaths = "-I../ "
	for headerDirectory in headerDirectories:
		compilerIncludePaths += "+I/" + str(headerDirectory) + " "

	for headerFile in headerFiles:
		os.system("clang-format -i -style=file " + headerFile)
	for sourceFile in sourceFiles:
		os.system("clang-format -i -style=file " + sourceFile)

formatSources()
