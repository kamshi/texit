#!/usr/bin/python

from pathlib import Path
import subprocess
import os

applyFixes = True
ignoreErrors = False
headerDirectories = []
headerFiles = []
sourceFiles = []
for path in Path('.').rglob('*.hpp'):
    filename = path.as_posix()
    if not filename.startswith("build"):
        headerDirectories.append(path.parent)
        headerFiles.append(filename)
for path in Path('.').rglob('*.cpp'):
    filename = path.as_posix()
    if not filename.startswith("build"):
        sourceFiles.append(filename)

headerDirectories = list(dict.fromkeys(headerDirectories))

compilerIncludePaths = "-I../ "
for headerDirectory in headerDirectories:
    compilerIncludePaths += "+I/" + str(headerDirectory) + " "

fixCommand = "-fix"
if ignoreErrors:
    fixCommand = "-fix-errors"

if applyFixes:
    for headerFile in headerFiles:
        os.system("clang-tidy " + fixCommand + " -header-filter='.*' --checks='-*,modernize-concat-nested-namespaces,modernize-make-shared,modernize-make-unique,modernize-raw-string-literal,modernize-redundant-void-arg,modernize-return-braced-init-list,modernize-shrink-to-fit,modernize-use-auto,modernize-use-default-member-init,modernize-use-emplace,modernize-use-equals-default,modernize-use-equals-delete,modernize-use-nodiscard,modernize-use-noexcept,modernize-use-override,modernize-use-trailing-return-type,modernize-use-nullptr,modernize-use-transparent-functors' " + headerFile + " -- -std=c++20 " + compilerIncludePaths)
    for sourceFile in sourceFiles:
        os.system("clang-tidy " + fixCommand + " -header-filter='.*' --checks='-*,modernize-concat-nested-namespaces,modernize-make-shared,modernize-make-unique,modernize-raw-string-literal,modernize-redundant-void-arg,modernize-return-braced-init-list,modernize-shrink-to-fit,modernize-use-auto,modernize-use-default-member-init,modernize-use-emplace,modernize-use-equals-default,modernize-use-equals-delete,modernize-use-nodiscard,modernize-use-noexcept,modernize-use-override,modernize-use-trailing-return-type,modernize-use-nullptr,modernize-use-transparent-functors' " + sourceFile + " -- -std=c++20 " + compilerIncludePaths)

    p = subprocess.Popen(['python', 'runClangFormat.py'], cwd='.')
    p.wait()
