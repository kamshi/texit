﻿/*
 * \file main.cpp
 * \copyright Copyright (c) 2022 Sebastian Jähne
 */

// texit
#include <texit.hpp>

// stl
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>

auto main(int argc, char** argv) -> int
{
    if(argc == 2)
    {
        std::ifstream file(argv[1]);
        if(!file.is_open())
        {
            std::cerr << "Could not open file '" << argv[1] << "'\n";
            return -1;
        }

        std::stringstream content;
        for(std::string line; std::getline(file, line);)
            content << line << '\n';
        // write directly to file
        {
            const auto result = texit::convertSourceToFile(
                content.str(), texit::InputFormat::LaTeX, "test.pdf", texit::OutputFormat::PDF,
                std::filesystem::copy_options::overwrite_existing);

            if(!result)
            {
                std::cerr << "Could not create PDF from '" << argv[1]
                          << "', reason: " << result.getDescription() << '\n';
                return -1;
            }
        }

        // first convert into bytes and then write to file
        {
            const auto [result, optionalBytes] = texit::convertSourceToBytes(
                content.str(), texit::InputFormat::LaTeX, texit::OutputFormat::PDF);
            if(!result)
            {
                std::cerr << "Could not create bytes for PDF from '" << argv[1]
                          << "', reason: " << result.getDescription() << '\n';
                return -1;
            }
            const auto& bytes = optionalBytes.value();

            std::ofstream pdf("test2.pdf", std::ios_base::binary);
            if(!pdf.is_open())
            {
                std::cerr << "Could not open '" << argv[1] << "' for writing.\n";
                return -1;
            }

            pdf.write(reinterpret_cast<const char*>(bytes.data()), bytes.size());
        }

        std::cout << "Successfully converted file '" << argv[1] << "' to pdf\n";
        return 0;
    }
    else
    {
        std::cerr << "No input file given.\n";
        return -1;
    }
}
